package org.nov0.model;

public class Note {

	private int id;
	private String label;
	private String author;

	public Note() {
	}

	public Note(int id, String label, String author) {
		this.id = id;
		this.label = label;
		this.author = author;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	@Override
	public String toString() {
		return "Note [id=" + id + ", label=" + label + ", author=" + author + "]";
	}

}