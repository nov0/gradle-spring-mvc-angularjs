package org.nov0.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {
	
	@RequestMapping("/")
	public String goToHome() {
		return "redirect:/resources/static/index.html";
	}

}
