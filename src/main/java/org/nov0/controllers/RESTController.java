package org.nov0.controllers;

import org.nov0.model.Note;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RESTController {
	
	@RequestMapping(value="/note", method=RequestMethod.GET)
	public ResponseEntity<Note> getNotes() {
		Note note = new Note(1, "First note", "Novo");
		System.out.println(note);
		return new ResponseEntity<Note>(note, HttpStatus.OK);
	}

}
